#!/bin/bash

yum update

#install PG-9.6-1C
curl http://1c.postgrespro.ru/keys/GPG-KEY-POSTGRESPRO-1C > GPG-KEY-POSTGRESPRO-1C
rpm --import GPG-KEY-POSTGRESPRO-1C
echo [postgrespro-1c] > /etc/yum.repos.d/postgrespro-1c.repo
echo name=Postgres Pro 1C repo >> /etc/yum.repos.d/postgrespro-1c.repo
echo baseurl=http://1c.postgrespro.ru/archive/2018_06_09/rpm/9.6/centos-7-x86_64 >> /etc/yum.repos.d/postgrespro-1c.repo
echo gpgcheck=1 >> /etc/yum.repos.d/postgrespro-1c.repo
echo enabled=1 >> /etc/yum.repos.d/postgrespro-1c.repo
yum makecache
yum install -y postgresql96-server-9.6.9-1.1C.x86_64

#Install python
yum install -y epel-release
yum install -y python36
python3.6 -m ensurepip
ln -s /usr/bin/python3.6 /usr/bin/python3
ln -s /usr/local/bin/pip3 /usr/bin/pip3
yum install -y python36-devel.x86_64
yum install gcc
pip3 install psycopg2-binary

#Install etcd
yum install -y etcd

#Install patroni[etcd]
pip3 install patroni[etcd]

mkdir -p /pgdata/patroni
cd /pgdata/patroni

chown -R postgres:postgres /pgdata

cp /tmp/etcd.conf /etc/etcd/etcd.conf
cp /tmp/node.yml /pgdata/patroni/node.yml

firewall-cmd --zone=public --permanent --add-port=2380/tcp &&\
firewall-cmd --zone=public --permanent --add-port=5432/tcp &&\
firewall-cmd --reload

echo sudo systemctl start etcd >> /home/vagrant/start.sh
echo sudo su - postgres -c \"patroni /pgdata/patroni/node.yml\" >> /home/vagrant/start.sh
chmod +x /home/vagrant/start.sh

echo export PATH=$PATH:/usr/pgsql-9.6/bin/:/usr/local/bin/ >> /var/lib/pgsql/.bash_profile

echo 192.168.100.100 node-1 >> /etc/hosts
echo 192.168.100.102 node-3 >> /etc/hosts
